# Follow The Sound

WebSocket server for follow the sound


# Get Started
You can either install Go or use docker

## With Go ToolChain
Start the server

```bash
go run .
```

The websocket server will listen on port `9000`

## With Docker-Compose
Start the server
```bash
docker-compose up --build
```

The websocket server will listen on port `5001`


## Author
- [kirinnee](https://kirinnee.dev)
