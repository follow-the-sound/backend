package message

type Response struct {
	Type       string      `json:"type"`
	Content    string      `json:"content"`
	Sender     string      `json:"sender"`
	SenderName string      `json:"senderName"`
	Meta       interface{} `json:"meta"`
	Original   interface{} `json:"original"`
}

type Request struct {
	Type    string      `json:"type"`
	Content string      `json:"content"`
	Meta    interface{} `json:"meta"`
}

func NewResponse(senderId, senderName string, req Request) Response {
	return Response{
		Type:       req.Type,
		Content:    req.Content,
		Sender:     senderId,
		SenderName: senderName,
		Meta:       req.Meta,
		Original:   req.Meta,
	}
}
