package message

import "math/rand"

type Init struct {
	Id    string `json:"id"`
	Name  string `json:"name"`
	HubId string `json:"hubId"`
}

type Join struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Members struct {
	Id    string `json:"id"`
	Name  string `json:"name"`
	Score uint32 `json:"score"`
	Color string `json:"color"`
}

type GameStart struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type GameEnd struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type HubInfo struct {
	Members []Members `json:"members"`
}

type CloseError struct {
	Type   string `json:"type"`
	Reason string `json:"reason"`
}

func NewError(err, reason string) Response {
	return Response{
		Type:       "error",
		Content:    "error",
		Sender:     "server",
		SenderName: "server",
		Meta: CloseError{
			Type:   err,
			Reason: reason,
		},
		Original: nil,
	}
}

func NewGame(req Request) Response {
	return Response{
		Type:       "newgame",
		Content:    "coordinates",
		Sender:     "server",
		SenderName: "server",
		Meta: GameStart{
			X: rand.Intn(100),
			Y: rand.Intn(100),
		},
		Original: req.Meta,
	}
}
