package util

import (
	"encoding/json"
	"github.com/google/uuid"
	"log"
)

func Guid() string {
	u, _ := uuid.NewUUID()
	id := u.String()
	return id
}

func RemoveIndex(s []string, index int) []string {
	return append(s[:index], s[index+1:]...)
}

func Log(any interface{}) {
	b, _ := json.Marshal(any)
	log.Println(string(b))
}
