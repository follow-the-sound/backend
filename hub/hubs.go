package hub

import (
	"gitlab.com/follow-the-sound/backend/message"
	"gitlab.com/follow-the-sound/backend/util"
	"math/rand"
)

var Hubs = make(map[string]*Hub)
var letterRunes = []rune("abcdefghijklmnopqrstuvwxyz0123456789")

func GenerateNewId() string {

	b := make([]rune, 5)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	s := string(b)

	if _, ok := Hubs[s]; ok {
		return GenerateNewId()
	}
	return s
}

func NewHub() *Hub {
	id := GenerateNewId()
	hub := &Hub{
		id:         id,
		Colors:     []string{"red", "purple", "blue", "green", "black"},
		Broadcast:  make(chan message.Response),
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}

	go hub.run()
	Hubs[id] = hub
	return hub
}

func GetHub(id string) *Hub {
	if hub, ok := Hubs[id]; ok {
		return hub
	}
	return nil
}

func JoinHub(id string, client *Client) {
	hub := Hubs[id]
	color := hub.Colors[0]
	hub.Colors = util.RemoveIndex(hub.Colors, 0)
	client.Color = color
	hub.Register <- client
	client.registerHub(hub)
}
