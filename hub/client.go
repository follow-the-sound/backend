package hub

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"gitlab.com/follow-the-sound/backend/message"
	"log"
	"time"
)

var GamesPlayed = 0

func BroadcastStatus(h *Hub, original interface{}) {
	ret := make([]message.Members, 0, 5)

	for _, ele := range h.Clients() {
		ret = append(ret, message.Members{
			Id:    ele.UserId,
			Name:  ele.UserName,
			Score: ele.Score,
			Color: ele.Color,
		})
	}
	h.Broadcast <- message.Response{
		Type:       "room-status",
		Content:    "meta",
		Sender:     "server",
		SenderName: "server",
		Meta:       message.HubInfo{ret},
		Original:   original,
	}
}

const (
	writeWait      = 10 * time.Second
	pongWait       = 60 * time.Second
	pingPeriod     = (pongWait * 9) / 10
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
)

type Client struct {
	UserId   string
	UserName string
	Score    uint32
	Color    string
	hubs     map[*Hub]bool
	conn     *websocket.Conn

	// Sending Channel
	Send chan message.Response
}

func (c *Client) readCycle() {
	defer func() {
		for h := range c.hubs {
			h.Unregister <- c
		}
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	// Clear the incoming websocket
	for {
		_, msg, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		var req message.Request
		err = json.Unmarshal(msg, &req)
		if err != nil {
			log.Println("Failed to unmarshal json", err)
		} else {
			var resp = message.NewResponse(c.UserId, c.UserName, req)
			switch req.Type {
			case "room-status":
				for h := range c.hubs {
					BroadcastStatus(h, req.Meta)
				}
			case "newgame":
				for h := range c.hubs {
					if h.Started {
						continue
					}
					h.Started = true
					h.Broadcast <- message.NewGame(req)
				}
			case "win":
				for h := range c.hubs {
					if !h.Started {
						continue
					}
					h.Started = false
					c.Score += 1

					h.Broadcast <- resp
					BroadcastStatus(h, req.Meta)
					GamesPlayed++
				}
			default:
				for h := range c.hubs {
					h.Broadcast <- resp
				}
			}

		}

	}
}

func (c *Client) writeCycle() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()

		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.Send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hubs closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}

			msg, err := json.Marshal(message)
			if err != nil {
				log.Println("Failed to unmarshal: ", err)
			}

			_, err = w.Write(msg)
			if err != nil {
				log.Println("Failed to send socket", err)
			}

			// Add queued chat messages to the current websocket message.
			n := len(c.Send)
			for i := 0; i < n; i++ {

				w.Write(newline)
				nextMessage := <-c.Send
				nextMsg, err := json.Marshal(nextMessage)
				if err != nil {
					log.Println("Failed to unmarshal: ", err)
				}
				_, err = w.Write(nextMsg)
				if err != nil {
					log.Println("Failed to send socket", err)
				}
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func CreateClient(conn *websocket.Conn, userId, userName string) *Client {
	client := &Client{
		hubs:     make(map[*Hub]bool),
		conn:     conn,
		Send:     make(chan message.Response),
		UserId:   userId,
		UserName: userName,
		Color:    "none",
	}

	log.Println("Client id: ", client.UserId, "Client name: ", client.UserName)

	// Start Cycles
	go client.writeCycle()
	go client.readCycle()

	return client
}

func (c *Client) registerHub(h *Hub) {
	c.hubs[h] = true
}
