package hub

import (
	"gitlab.com/follow-the-sound/backend/message"
	"log"
)

type Hub struct {
	id      string
	clients map[*Client]bool
	Started bool

	Colors []string

	// Broadcasting channel
	Broadcast chan message.Response

	// External registering and unregistering mechanics
	Register   chan *Client
	Unregister chan *Client
}

func (h *Hub) Members() int {
	return len(h.clients)
}

func (h *Hub) Id() string {
	return h.id
}

func (h *Hub) Clients() []*Client {

	ret := make([]*Client, 0, 5)
	for k := range h.clients {
		ret = append(ret, k)
	}
	return ret
}

// Auto register and deregister clients, and broadcast messages
func (h *Hub) run() {
	for {

		select {
		case client := <-h.Register:
			h.clients[client] = true
		case client := <-h.Unregister:
			if _, ok := h.clients[client]; ok {
				log.Println("Unregistering")
				msg := message.Response{
					Type:       "quit",
					Content:    "meta",
					Sender:     "server",
					SenderName: "server",
					Meta: message.Join{
						Id:   client.UserId,
						Name: client.UserName,
					},
					Original: nil,
				}
				h.Colors = append(h.Colors, client.Color)
				delete(h.clients, client)
				close(client.Send)
				go func() {
					h.Broadcast <- msg
				}()
			}
		case message := <-h.Broadcast:
			for client := range h.clients {
				select {
				case client.Send <- message:
				}
			}
		}
		log.Println(len(h.clients))
		if len(h.clients) == 0 {
			break
		}

	}
	log.Println(h.id + " is deleted")
	delete(Hubs, h.id)
}
